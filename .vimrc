execute pathogen#infect()
syntax on

autocmd ColorScheme * highlight ExtraWhitespace ctermbg=green guibg=red
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/
if version >= 702
  autocmd BufWinLeave * call clearmatches()
endif

set nocompatible

set listchars=eol:¬,tab:>-
set list
set colorcolumn=80
set tabstop=2
set softtabstop=2
set expandtab
set textwidth=0
set nu
set ruler
set encoding=utf-8
set fileencoding=utf-8
set backspace=2
colorscheme molokai
set cursorline
hi CursorLine term=bold cterm=bold guibg=Grey40
set autoindent

nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-l> :wincmd l<CR>
